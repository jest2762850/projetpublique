package testWebService;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class GURU {
	
	WebDriver driver;
	WebElement bouton;
	String mdp = "mercury";
	String login = "mercury";
	

	@Test
	public void test() {
		System.setProperty("webdriver.gecko.driver" , "src/test/resources/driver/geckodriver.exe");
		driver = new FirefoxDriver();
		
		
		System.out.println("************Acc�s au Site*****************");
		driver.get("https://demo.guru99.com/test/newtours");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
		
		
		driver.switchTo().frame("gdpr-consent-notice");
		bouton = driver.findElement(By.xpath("//button[@id='save']"));
		bouton.click();
		
		driver.switchTo().defaultContent();
		
		driver.findElement(By.name("userName")).sendKeys(login);

		driver.findElement(By.name("password")).sendKeys(mdp);
		bouton = driver.findElement(By.name("submit"));
		bouton.click();
		
		System.out.println("************    OK     ***********");
		
		
		
		
	}
	
	@After
	public void close() {
	
		driver.quit();	
	}

}