package ChallSele;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;


public class chall {

	
	public ArrayList<Map<String, String>> loadCsvJDD (String fileName) throws IOException {
		String csvFilePath = "src/test/resources/csv/" + fileName + ".csv";
		listJDD = new ArrayList<>();
		List<String[]> list =
		Files.lines(Paths.get(csvFilePath))
		.map(line -> line.split("\\\\r?\\\\n"))
		.collect(Collectors.toList());
		for (int j = 1; j < list.size(); j++) {
			Map<String, String> jdd = new HashMap<>();
			String[] titres = list.get(0)[0].split(",");
			String[] val = list.get(j)[0].split((","));
		for (int i = 0; i < titres.length; i++) {
			jdd.put(titres[i], val[i]);
				}
			listJDD.add(jdd);
				}
			return listJDD;
		}
	
	ArrayList<Map<String, String>> listJDD;
	WebDriver driver;
	WebElement start;
	WebElement bouton;
	
	
	@Test
	public void test() throws Exception {
		System.setProperty("webdriver.gecko.driver" , "src/test/resources/driver/geckodriver.exe");
		driver = new FirefoxDriver();
		
		System.out.println("************Acc�s au Site*****************");
		driver.get("https://www.rpachallenge.com/");
		driver.manage().window().maximize();
		start = driver.findElement(By.xpath("//div/button[@class='waves-effect col s12 m12 l12 btn-large uiColorButton']"));
		start.click();
		
		
		ArrayList<Map<String, String>> dataList = loadCsvJDD("challenge");
		
		for (int i = 0; i<10 ; i++) {
        Map<String, String> data = dataList.get(i);
	
	
	
	
	
        String Fname = data.get("First Name");
        String Lname = data.get("Last Name"); 
        String Cname = data.get("Company Name"); 
        String Role = data.get("Role in Company"); 
        String Adress = data.get("Address"); 
        String Email = data.get("Email"); 
        String Pnumber = data.get("Phone Number"); 
        
        
        
        
        
        
        
        
        driver.findElement(By.xpath("//div/input[@ng-reflect-name='labelFirstName']")).sendKeys(Fname);
		driver.findElement(By.xpath("//div/input[@ng-reflect-name='labelLastName']")).sendKeys(Lname);
		driver.findElement(By.xpath("//div/input[@ng-reflect-name='labelCompanyName']")).sendKeys(Cname);
		driver.findElement(By.xpath("//div/input[@ng-reflect-name='labelRole']")).sendKeys(Role);
		driver.findElement(By.xpath("//div/input[@ng-reflect-name='labelAddress']")).sendKeys(Adress);
		driver.findElement(By.xpath("//div/input[@ng-reflect-name='labelEmail']")).sendKeys(Email);
		driver.findElement(By.xpath("//div/input[@ng-reflect-name='labelPhone']")).sendKeys(Pnumber);
		bouton = driver.findElement(By.xpath("//input[@class='btn uiColorButton']"));
		bouton.click();
		}
}
}