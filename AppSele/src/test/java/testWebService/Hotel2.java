package testWebService;




import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.lang.annotation.Target;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;

	public class Hotel2 {
		
		WebDriver driver;
		WebElement bouton;
		WebElement source;
		WebElement target;
			
		
		@Test
		public void test() throws InterruptedException {
			System.setProperty("webdriver.gecko.driver" , "src/test/resources/driver/geckodriver.exe");
			driver = new FirefoxDriver();
			
			
			System.out.println("************Acc�s au Site*****************");
			driver.get("http://localhost/TutorialHtml5HotelPhp/");
			driver.manage().window().maximize();
			
			bouton = driver.findElement(By.xpath("/html/body/div[4]/div[2]/div[2]/div[3]/div[3]/div/div[2]/div[1]"));
			bouton.click();
			driver.switchTo().frame(0);
			driver.findElement(By.xpath("//body/form/h1[contains(.,'New Reservation')]")).isDisplayed();
			driver.findElement(By.name("name")).sendKeys("resa 1");
			bouton = driver.findElement(By.xpath("//div/input[@type='submit']"));
			bouton.click();
			driver.switchTo().defaultContent();
			driver.findElement(By.xpath("//div[@class='scheduler_default_event scheduler_default_event_line0'][contains(.,'resa 1 (12/1/2023 - 12/2/2023)')]")).isDisplayed();
			bouton = driver.findElement(By.xpath("//div[@class='scheduler_default_event scheduler_default_event_line0'][contains(.,'resa 1 (12/1/2023 - 12/2/2023)')]"));
			System.out.println(bouton.getText());
			
			System.out.println("************    OK     ***********");
			System.out.println("");
			
			
			
			
			System.out.println("************D�placer la r�sa 1*****************");
			
			source = driver.findElement(By.xpath("//div[@class='scheduler_default_event scheduler_default_event_line0'][contains(.,'resa 1 (12/1/2023 - 12/2/2023)')]"));
			target = driver.findElement(By.xpath("/html/body/div[4]/div[2]/div[2]/div[3]/div[3]/div/div[2]/div[6]"));
			Actions a = new Actions(driver);
			a.dragAndDrop(source,target).build().perform();
			driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
			
			driver.findElement(By.xpath("//div[@class='scheduler_default_message']")).isDisplayed();
			
			Thread.sleep(7000);
			
			assertFalse(driver.findElement(By.xpath("//div[@class='scheduler_default_message']")).isDisplayed());
			
			
			
			
			
			
			
		
		}
		
		
		
		
		
	@After
	public void close() {
	
		//driver.quit();	
	}
}