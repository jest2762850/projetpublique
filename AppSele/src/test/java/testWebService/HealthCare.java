package testWebService;

import java.awt.Menu;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

public class HealthCare {
	
	WebDriver driver;
	WebElement bouton;
	WebElement login;
	WebElement logOut;
	WebElement forLog;
	WebElement menu;
	
	@Test
	public void test() {
		System.setProperty("webdriver.gecko.driver" , "src/test/resources/driver/geckodriver.exe");
		driver = new FirefoxDriver();
		
		
		driver.get("https://katalon-demo-cura.herokuapp.com");
		driver.manage().window().maximize();
		
		//Initiation du IMPLICIT
		//driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		
		forLog = driver.findElement(By.xpath("//.[@href='./profile.php#login\']"));
		forLog.click();
		
		
		
		
		driver.findElement(By.name("username")).sendKeys("John Doe");
		driver.findElement(By.name("password")).clear();
		driver.findElement(By.name("password")).sendKeys("ThisIsNotAPassword");
		
		

		//Initiation de EXPLICIT
		//WebDriverWait wait = new WebDriverWait(driver, 10);
		
		//Initiation de FLUENT
		//Wait<WebDriver> wait = new FluentWait<WebDriver>(driver);
		
		
		//Mise en �chec IMPLICIT
		// driver.findElement(By.xpath("//*[@id=\"WelcomeContent\"][contains(.,'Welcome')]")).isDisplayed();
		
		//On constate que l'implicit Wait va attendre 10 sec puis passer au test suivant m�me si il est en echec.
		
		//Mise en �chec de EXPLICIT
		//WebElement webElement = wait.until(ExpectedConditions.elementToBeClickable(By.id("btnLogin")));
		
		//On constate que l'EXPLICIT wait va attendre 10 sec puis passer au test d'apr�s
		
		login = driver.findElement(By.xpath("//.[@id='btn-login']"));
		login.click();
		
		
		menu = driver.findElement(By.xpath("//.[@id='menu-toggle']"));
		menu.click();	
				
		
		logOut = driver.findElement(By.xpath("//.[@href='authenticate.php?logout']"));
		logOut.click();
	}

	
	@After
	public void close() {
	
		driver.quit();	
	}

}