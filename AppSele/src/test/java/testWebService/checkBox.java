package testWebService;



import java.util.List;

import org.junit.After;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

public class checkBox {
		
		WebDriver driver;
		WebElement bouton;
		
		
			
		
		@Test
		public void test() throws InterruptedException {
			System.setProperty("webdriver.gecko.driver" , "src/test/resources/driver/geckodriver.exe");
			driver = new FirefoxDriver();
			
			System.out.println("************Acc�s au Site*****************");
			driver.get("https://petstore.octoperf.com/actions/Catalog.action");
			driver.manage().window().maximize();
			bouton = driver.findElement(By.linkText("Sign In"));
//			bouton = driver.findElement(By.xpath("//div[1]/div[2]/div/a[2]"));
			bouton.click();
			driver.findElement(By.name("username")).sendKeys("j2ee");
			driver.findElement(By.name("password")).clear();
			driver.findElement(By.name("password")).sendKeys("j2ee");
			bouton = driver.findElement(By.xpath("//div[2]/div/form/input"));
			bouton.click();
			driver.findElement(By.xpath("//*[@id=\"WelcomeContent\"][contains(.,'Welcome')]")).isDisplayed();
			driver.findElement(By.xpath("//div[1]/div[2]/div/a[2][contains(.,'Sign Out')]")).isDisplayed();
			System.out.println("***********     OK      ************");
			System.out.println("");
			
			
			System.out.println("************Acc�s � MyAccount*****************");
			bouton = driver.findElement(By.linkText("My Account"));
			bouton.click();
			driver.findElement(By.xpath("//form/h3[3]")).isDisplayed();
			
			System.out.println("***********     OK      ************");
			System.out.println("");
			
			
			System.out.println("************S�l�ction Japanese et Reptiles*****************");
			
			WebElement menuLangue = driver.findElement(By.name("account.languagePreference"));
			Select selectLangue = new Select(menuLangue);
			selectLangue.selectByValue("japanese");
			
			
			WebElement menuAnimal = driver.findElement(By.name("account.favouriteCategoryId"));
			Select selectAnimal = new Select(menuAnimal);
			selectAnimal.selectByValue("REPTILES");
			WebElement save = driver.findElement(By.name("editAccount"));
			save.click();
			
			driver.findElement(By.xpath("//*[@name='account.languagePreference'][contains(.,'japanese')]")).isDisplayed();		//a modif car ne v�rifie pas r��element
			driver.findElement(By.xpath("//*[@name='account.favouriteCategoryId'][contains(.,'REPTILES')]")).isDisplayed();		//a modif car ne v�rifie pas r��element
			
			System.out.println("***********     OK      ************");
			System.out.println("");
			
			System.out.println("************V�rifier que Enable My list et My Banner sont s�l�ctionner par defaut*****************");
			
			List<WebElement> checkboxOptions1 = driver.findElements(By.name("account.listOption"));
			List<WebElement> checkboxOptions2 = driver.findElements(By.name("account.bannerOption"));
			if (checkboxOptions1.get(0).isSelected()) {
				System.out.println("MyList est bien s�l�ctionn�");
				
			}
			else {
				System.out.println("L'option MyList n'est pas s�l�ctionner");
			}
			if (checkboxOptions2.get(0).isSelected()) {
				System.out.println("MyBanner est bien s�l�ctionn�");
				
			}
			else {
				System.out.println("L'option Mybanner n'est pas s�l�ctionner");
			}
			System.out.println("***********     OK      ************");
			System.out.println("");
			
			
			System.out.println("************D�s�l�ctionner Enable My list*****************");
			List<WebElement> checkboxOptionsNON = driver.findElements(By.name("account.listOption"));
			checkboxOptionsNON.get(0).click();
			
			if (checkboxOptionsNON.get(0).isSelected()) {
				System.out.println("MyList est bien s�l�ctionn�");
				
			}
			else {
				System.out.println("L'option MyList n'est pas s�l�ctionner");
				
			}
			System.out.println("***********     OK      ************");
			System.out.println("");
			
			
}
		
		
	
		@After
		public void close() {
		
			driver.quit();	
		}

	
}
