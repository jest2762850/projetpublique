package testWebService;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class petStoreTest {
	
	WebDriver driver;
	WebElement bouton;
	String prix1;
	String prix2;
	double prix11;
	double prix22;
	
		
	
	@Test
	public void test() {
		System.setProperty("webdriver.gecko.driver" , "src/test/resources/driver/geckodriver.exe");
		driver = new FirefoxDriver();
		
		System.out.println("************Acc�s au Site*****************");
		driver.get("https://petstore.octoperf.com/actions/Catalog.action");
		driver.manage().window().maximize();
		bouton = driver.findElement(By.xpath("//div[1]/div[2]/div/a[2]"));
		bouton = driver.findElement(By.linkText("Sign In"));
	//	bouton = driver.findElement(By.xpath("//.[@href='/actions/Account.action?signonForm=']"));
		bouton.click();
		driver.findElement(By.name("username")).sendKeys("j2ee");
		driver.findElement(By.name("password")).clear();
		driver.findElement(By.name("password")).sendKeys("j2ee");
		bouton = driver.findElement(By.xpath("//div[2]/div/form/input"));
		bouton.click();
		driver.findElement(By.xpath("//*[@id=\"WelcomeContent\"][contains(.,'Welcome')]")).isDisplayed();
		driver.findElement(By.xpath("//div[1]/div[2]/div/a[2][contains(.,'Sign Out')]")).isDisplayed();
		System.out.println("***********     OK      ************");
		System.out.println("");
		
		
		
		
		System.out.println("*****Test click poisson*******");
		bouton =driver.findElement(By.xpath("//div[2]/div[2]/div[1]/div/a[1]"));
		bouton.click();
		driver.findElement(By.xpath("//table/tbody/tr[1]/th[1][contains(.,'Product ID')]")).isDisplayed();
		System.out.println("************    OK     ***********");
		System.out.println("");
		
		
		
		System.out.println("************S�l�ction produit*****************");
		bouton = driver.findElement(By.xpath("/html/body/div[2]/div[2]/table/tbody/tr[4]/td[1]/a"));
		bouton.click();
		System.out.println("************     OK     ************");
		System.out.println("");
		
		System.out.println("************Ajout Item Panier*****************");
		bouton = driver.findElement(By.xpath("//table/tbody/tr[2]/td[5]/a"));
		bouton.click();
		driver.findElement(By.xpath("//div[1]/h2[contains(.,'Shopping Cart')]")).isDisplayed();
		System.out.println("***********      OK      ***********");
		System.out.println("");
		
		
		
		System.out.println("************Changer Quantit�*****************");
		driver.findElement(By.name("EST-4")).clear();
		driver.findElement(By.name("EST-4")).sendKeys("2");
		bouton = driver.findElement(By.xpath("//form/table/tbody/tr[3]/td[1]/input"));
		bouton.click();
		prix1 = driver.findElement(By.xpath("//form/table/tbody/tr[2]/td[6]")).getText();
		prix2= driver.findElement(By.xpath("//form/table/tbody/tr[2]/td[7]")).getText();
		
		 String prix111 = prix1.replace("$", "");
		 String prix222 = prix2.replace("$", "");
		 
		 
	
	
//		prix11 = Double.parseDouble(prix111);
//	
//		prix22 = Double.parseDouble(prix222);
//		
//		prix11 = prix11 * 2;
		
		
		assertEquals("Le prix n'est pas le bon",prix11 = Double.parseDouble(prix111) * 2 ,prix22 = Double.parseDouble(prix222),0);
		
		System.out.println("**********    OK             *************");
		
	
	}
	
	
	@After
	public void close() {
	
		driver.quit();	
	}

}
