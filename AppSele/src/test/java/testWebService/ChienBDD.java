package testWebService;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


import org.junit.After;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class ChienBDD {
	
	
	WebDriver driver;
	WebElement bouton;
	ArrayList<Map<String, String>> listJDD;
	String nomFichier = "Chien";	
	
	public ArrayList<Map<String, String>> loadCsvJDD (String fileName) throws IOException {
		String csvFilePath = "src/test/resources/csv/" + fileName + ".csv";
		listJDD = new ArrayList<>();
		List<String[]> list =
		Files.lines(Paths.get(csvFilePath))
		.map(line -> line.split("\\\\r?\\\\n"))
		.collect(Collectors.toList());
		for (int j = 1; j < list.size(); j++) {
		Map<String, String> jdd = new HashMap<>();
		String[] titres = list.get(0)[0].split(",");
		String[] val = list.get(j)[0].split((","));
		for (int i = 0; i < titres.length; i++) {
		jdd.put(titres[i], val[i]);
		}
		listJDD.add(jdd);
		}
		return listJDD;
		}
	
	public static void TextFileWriting(String pfile, String ptext) throws IOException {
		try {
		FileOutputStream outputStream = new FileOutputStream(pfile);
		OutputStreamWriter outputStreamWriter = new OutputStreamWriter(outputStream, "UTF-8");
		BufferedWriter bufferedWriter = new BufferedWriter(outputStreamWriter);
		bufferedWriter.write(ptext);
		bufferedWriter.close();
		} catch (IOException e) {
		System.out.println("Un probl�me avec le fichier " + pfile);
		throw e;
		}
		}
	
	
	@Test
	public void test() throws InterruptedException, IOException {
		System.setProperty("webdriver.gecko.driver" , "src/test/resources/driver/geckodriver.exe");
		driver = new FirefoxDriver();
		
		ArrayList<Map<String, String>> dataList = loadCsvJDD("BDDCHIEN");
        Map<String, String> data = dataList.get(0);

        String log = data.get("login"); // Contient "j2ee"
        String mdp = data.get("mdp"); // Contient "j2ee"
        System.out.println(log);
        System.out.println(mdp);
        
        
		
		System.out.println("************Acc�s au Site*****************");
		driver.get("https://petstore.octoperf.com/actions/Catalog.action");
		driver.manage().window().maximize();
		bouton = driver.findElement(By.linkText("Sign In"));
//		bouton = driver.findElement(By.xpath("//div[1]/div[2]/div/a[2]"));
		bouton.click();
		driver.findElement(By.name("username")).sendKeys(log);
		driver.findElement(By.name("password")).clear();
		driver.findElement(By.name("password")).sendKeys(mdp);
		bouton = driver.findElement(By.xpath("//div[2]/div/form/input"));
		bouton.click();
		driver.findElement(By.xpath("//*[@id=\"WelcomeContent\"][contains(.,'Welcome')]")).isDisplayed();
		driver.findElement(By.xpath("//div[1]/div[2]/div/a[2][contains(.,'Sign Out')]")).isDisplayed();
		System.out.println("***********     OK      ************");
		System.out.println("");
		
		bouton = driver.findElement(By.xpath("//div/a[@href='/actions/Catalog.action?viewCategory=&categoryId=DOGS']"));
		bouton.click();
		
		bouton = driver.findElement(By.linkText("K9-DL-01"));
		bouton.click();
		TextFileWriting("src/test/resources/csv/"+nomFichier+".txt", driver.findElement(By.xpath("//div[2]/h2")).getText());
}
	
	@After
	public void close() {
	
		driver.quit();	
	}
}