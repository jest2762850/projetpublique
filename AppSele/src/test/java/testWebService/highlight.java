package testWebService;

import java.io.File;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;
public class highlight {
	
	 public void myClick(WebElement element){
	        JavascriptExecutor js=(JavascriptExecutor) driver;
	        js.executeScript("arguments[0].scrollIntoView();",element);
	        js.executeScript("arguments[0].setAttribute('style', 'background: yellow; border: 2px solid red;');", element);
	        element.click();

	    }
	 
	 public void mySendKeys(WebElement element,String text){
	        JavascriptExecutor js=(JavascriptExecutor) driver;
	        js.executeScript("arguments[0].scrollIntoView();",element);
	        js.executeScript("arguments[0].setAttribute('style', 'background: yellow; border: 2px solid red;');", element);
	        element.sendKeys(text);

	    }

	 public void highLighterMethod(WebElement element){
		 JavascriptExecutor js = (JavascriptExecutor) driver;
		 js.executeScript("arguments[0].scrollIntoView();",element);
		 js.executeScript("arguments[0].setAttribute('style', 'background: yellow; border: 2px solid red;');",
		 element);
		 }

	 public static void takeSnapShot(WebDriver webdriver,String fileWithPath) throws Exception{
		//Convert web driver object to TakeScreenshot
		TakesScreenshot scrShot =((TakesScreenshot)webdriver);
		//Call getScreenshotAs method to create image file
		File SrcFile=scrShot.getScreenshotAs(OutputType.FILE);
		//Move image file to new destination
		File DestFile=new File(fileWithPath);
		//Copy file at destination
		FileUtils.copyFile(SrcFile, DestFile);
		}

	
			
			WebDriver driver;
			WebElement bouton;
			WebElement logElement;
			WebElement mdpElement;
			WebElement signIn;
			WebElement signOut;
			WebElement logIn;
			WebElement myAccount;
		
			
				
			
			@Test
			public void test() throws Exception {
				System.setProperty("webdriver.gecko.driver" , "src/test/resources/driver/geckodriver.exe");
				driver = new FirefoxDriver();
				
				System.out.println("************Acc�s au Site*****************");
				driver.get("https://petstore.octoperf.com/actions/Catalog.action");
				driver.manage().window().maximize();
				signIn = driver.findElement(By.linkText("Sign In"));
//				bouton = driver.findElement(By.xpath("//div[1]/div[2]/div/a[2]"));
				
				
				
				myClick(signIn);
//				bouton.click();
				
				
				logElement = driver.findElement(By.name("username"));
				mdpElement = driver.findElement(By.name("password"));
				
				mySendKeys(logElement, "j2ee");
				mdpElement.clear();
				mySendKeys(mdpElement, "j2ee");	
				takeSnapShot(driver,"src/test/resources/screenShot/dogs/screen3.png");
//				driver.findElement(By.name("username")).sendKeys("j2ee");
//				driver.findElement(By.name("password")).clear();
//				driver.findElement(By.name("password")).sendKeys("j2ee");
				logIn = driver.findElement(By.xpath("//div[2]/div/form/input"));
				myClick(logIn);
				driver.findElement(By.xpath("//*[@id=\"WelcomeContent\"][contains(.,'Welcome')]")).isDisplayed();
				driver.findElement(By.xpath("//div[1]/div[2]/div/a[2][contains(.,'Sign Out')]")).isDisplayed();
				System.out.println("***********     OK      ************");
				System.out.println("");
				
				
				System.out.println("************Acc�s � MyAccount*****************");
				myAccount = driver.findElement(By.linkText("My Account"));
				myClick(myAccount);
				driver.findElement(By.xpath("//form/h3[3]")).isDisplayed();
				takeSnapShot(driver,"src/test/resources/screenShot/dogs/screen4.png");
				
				System.out.println("***********     OK      ************");
				System.out.println("");
				
				
				System.out.println("************S�l�ction Japanese et Reptiles*****************");
				
				WebElement menuLangue = driver.findElement(By.name("account.languagePreference"));
				Select selectLangue = new Select(menuLangue);
				selectLangue.selectByValue("japanese");
				highLighterMethod(menuLangue);
				
				
				WebElement menuAnimal = driver.findElement(By.name("account.favouriteCategoryId"));
				Select selectAnimal = new Select(menuAnimal);
				selectAnimal.selectByValue("REPTILES");
				highLighterMethod(menuAnimal);
				takeSnapShot(driver,"src/test/resources/screenShot/dogs/screen.png");
				
				WebElement save = driver.findElement(By.name("editAccount"));
				myClick(save);
				
				driver.findElement(By.xpath("//*[@name='account.languagePreference'][contains(.,'japanese')]")).isDisplayed();		//a modif car ne v�rifie pas r��element
				driver.findElement(By.xpath("//*[@name='account.favouriteCategoryId'][contains(.,'REPTILES')]")).isDisplayed();		//a modif car ne v�rifie pas r��element
				
				System.out.println("***********     OK      ************");
				System.out.println("");
				
				System.out.println("************V�rifier que Enable My list et My Banner sont s�l�ctionner par defaut*****************");
				
				List<WebElement> checkboxOptions1 = driver.findElements(By.name("account.listOption"));
				List<WebElement> checkboxOptions2 = driver.findElements(By.name("account.bannerOption"));
				if (checkboxOptions1.get(0).isSelected()) {
					System.out.println("MyList est bien s�l�ctionn�");
					
				}
				else {
					System.out.println("L'option MyList n'est pas s�l�ctionner");
				}
				if (checkboxOptions2.get(0).isSelected()) {
					System.out.println("MyBanner est bien s�l�ctionn�");
					
				}
				else {
					System.out.println("L'option Mybanner n'est pas s�l�ctionner");
				}
				System.out.println("***********     OK      ************");
				System.out.println("");
				
				
				System.out.println("************D�s�l�ctionner Enable My list*****************");
				List<WebElement> checkboxOptionsNON = driver.findElements(By.name("account.listOption"));
				myClick(checkboxOptionsNON.get(0));
				
				if (checkboxOptionsNON.get(0).isSelected()) {
					System.out.println("MyList est bien s�l�ctionn�");
					
				}
				else {
					System.out.println("L'option MyList n'est pas s�l�ctionner");
					
				}
				System.out.println("***********     OK      ************");
				System.out.println("");
				takeSnapShot(driver,"src/test/resources/screenShot/dogs/screen2.png");
				
	}
			
			
		
			@After
			public void close() {
			
				//driver.quit();	
			}

		
	}


