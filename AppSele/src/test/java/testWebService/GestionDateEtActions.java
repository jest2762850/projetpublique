package testWebService;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.lang.annotation.Target;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
//import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

	public class GestionDateEtActions {
		
		WebDriver driver;
		WebElement bouton;
		WebElement source;
		WebElement target;
		WebElement croix;
		String dateA = afficherLaDateDuJourEnString()+"T12:00:00";
		String dateB = afficherLaDateDeDemainEnString()+"T12:00:00";
		String dateAA = afficherLaDateDuJourEnString();
		String dateBB = afficherLaDateDeDemainEnString();
		String nomResa = "resa-1";
		LocalDate newdateJourAA = LocalDate.now().plusDays(1);
		LocalDate newdateJourBB = LocalDate.now().plusDays(2);
		
		
		
		public String afficherLaDateDuJourEnString () {
			LocalDate date = LocalDate.now();
			DateTimeFormatter formatDate =
			DateTimeFormatter.ofPattern("yyyy-MM-dd");
			String dateJour = date.format(formatDate);
			return dateJour;
			}
		
		public String afficherLaDateDeDemainEnString () {
			LocalDate date1 = LocalDate.now().plusDays(1);
			DateTimeFormatter formatDate =
			DateTimeFormatter.ofPattern("yyyy-MM-dd");
			String dateJour2 = date1.format(formatDate);
			return dateJour2;
			}
		
		public String afficherLaDate (LocalDate newdate) {
			newdate = LocalDate.now();
			DateTimeFormatter formatDate =
			DateTimeFormatter.ofPattern("yyyy-MM-dd");
			String dateJour = newdate.format(formatDate);
			return dateJour;
			}
		//MODIF EN FORMAT MM-dd-YYYY
		public String modifFormatDate (String newFormDate) {
			LocalDate newFormDate1 = LocalDate.now();
			DateTimeFormatter formatDate =
			DateTimeFormatter.ofPattern("MM-dd-yyyy");
			String dateJour = newFormDate1.format(formatDate);
			return dateJour;
			}
		
		
		
		
		
		@Test
		public void test() throws InterruptedException {
			System.setProperty("webdriver.gecko.driver" , "src/test/resources/driver/geckodriver.exe");
			driver = new FirefoxDriver();
			
			
			System.out.println("************Acc�s au Site*****************");
			driver.get("http://localhost/TutorialHtml5HotelPhp/");
			driver.manage().window().maximize();
			System.out.println("************    OK     ***********");
			System.out.println("");
			
			System.out.println("************ cr�ation r�sa jour J *****************");
			bouton = driver.findElement(By.xpath("/html/body/div[4]/div[2]/div[2]/div[3]/div[3]/div/div[2]/div[22]"));

			bouton.click();
			driver.switchTo().frame(0);
			driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
			driver.findElement(By.xpath("//body/form/h1[contains(.,'New Reservation')]")).isDisplayed();
			driver.findElement(By.name("name")).sendKeys(nomResa);
			driver.findElement(By.name("start")).clear();
			driver.findElement(By.name("end")).clear();
			driver.findElement(By.name("start")).sendKeys(dateA);
			driver.findElement(By.name("end")).sendKeys(dateB);
			bouton = driver.findElement(By.xpath("//div/input[@type='submit']"));
			bouton.click();
			
			driver.switchTo().defaultContent();
			
			System.out.println("************    OK     ***********");
			System.out.println("");
			
		System.out.println(modifFormatDate(dateAA));
		System.out.println(afficherLaDate(newdateJourAA));
		
			System.out.println("************ Changer resa-1 au lendemain*****************");
			source = driver.findElement(By.linkText("[contains(.,'"+nomResa+" ("+modifFormatDate(dateAA)+" - "+modifFormatDate(dateBB)+")')]"));
			//bouton = driver.findElement(By.xpath("/html/body/div[4]/div[2]/div[2]/div[3]/div[3]/div/div[2]/div[22]"));
			source.click();
			
			driver.switchTo().frame(0);
			
			
			
			driver.findElement(By.name("start")).clear();
			driver.findElement(By.name("end")).clear();
			driver.findElement(By.name("start")).sendKeys(afficherLaDate(newdateJourAA));
			driver.findElement(By.name("end")).sendKeys(afficherLaDate(newdateJourBB));
			bouton = driver.findElement(By.xpath("//div/input[@type='submit']"));
			bouton.click();
			
//			System.out.println("************supprimer la resa 1****************");
//			
//			source = driver.findElement(By.xpath("//div[@class='scheduler_default_event scheduler_default_event_line0'][contains(.,'resa 1 (12/1/2023 - 12/2/2023)')]"));
//		
//			
//			
//			Actions mouseHover = new Actions(driver);
//			mouseHover.moveToElement(source).build().perform();
//			Thread.sleep(2000);
//	
//		   driver.findElement(By.xpath("/html/body/div[4]/div[2]/div[2]/div[3]/div[3]/div/div[9]/div/div[5]")).click();
//			
//			
//		   assertTrue(driver.findElement(By.xpath("//div[@class='scheduler_default_message'][contains(.,'Deleted.')]")).isDisplayed());
//		   Thread.sleep(7000);
//		   assertFalse(driver.findElement(By.xpath("//div[@class='scheduler_default_message'][contains(.,'Deleted.')]")).isDisplayed());
//		   
//		   
//		   
//		   System.out.println("******************          OK           *********************");
			
		}
		
		
		
		
		
	@After
	public void close() {
	
		//driver.quit();	
	}
}