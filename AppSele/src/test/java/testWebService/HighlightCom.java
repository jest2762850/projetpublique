
package testWebService;

import java.io.File;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

public class HighlightCom {

    WebDriver driver;
    WebElement bouton;
    WebElement logElement;
    WebElement mdpElement;
    WebElement signIn;
    WebElement signOut;
    WebElement logIn;
    WebElement myAccount;

    // Méthode pour simuler un clic en mettant en surbrillance l'élément
    public void myClick(WebElement element) {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].scrollIntoView();", element);
        js.executeScript("arguments[0].setAttribute('style', 'background: yellow; border: 2px solid red;');", element);
        element.click();
    }

    // Méthode pour simuler la saisie de texte en mettant en surbrillance l'élément
    public void mySendKeys(WebElement element, String text) {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].scrollIntoView();", element);
        js.executeScript("arguments[0].setAttribute('style', 'background: yellow; border: 2px solid red;');", element);
        element.sendKeys(text);
    }

    // Méthode pour mettre en surbrillance un élément
    public void highLighterMethod(WebElement element) {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].scrollIntoView();", element);
        js.executeScript("arguments[0].setAttribute('style', 'background: yellow; border: 2px solid red;');", element);
    }

    // Méthode pour prendre une capture d'écran
    public static void takeSnapShot(WebDriver webdriver, String fileWithPath) throws Exception {
        TakesScreenshot scrShot = ((TakesScreenshot) webdriver);
        File SrcFile = scrShot.getScreenshotAs(OutputType.FILE);
        File DestFile = new File(fileWithPath);
        FileUtils.copyFile(SrcFile, DestFile);
    }

    @Test
    public void test() throws Exception {
        System.setProperty("webdriver.gecko.driver", "src/test/resources/driver/geckodriver.exe");
        driver = new FirefoxDriver();

        System.out.println("************Accès au Site*****************");
        driver.get("https://petstore.octoperf.com/actions/Catalog.action");
        driver.manage().window().maximize();
        signIn = driver.findElement(By.linkText("Sign In"));

        // Utilisation de la méthode personnalisée pour le clic
        myClick(signIn);

        logElement = driver.findElement(By.name("username"));
        mdpElement = driver.findElement(By.name("password"));

        // Utilisation de la méthode personnalisée pour la saisie de texte
        mySendKeys(logElement, "j2ee");
        mdpElement.clear();
        mySendKeys(mdpElement, "j2ee");
        takeSnapShot(driver, "src/test/resources/screenShot/dogs/screen3.png");

        logIn = driver.findElement(By.xpath("//div[2]/div/form/input"));
        myClick(logIn);
        driver.findElement(By.xpath("//*[@id=\"WelcomeContent\"]" +
                "[contains(.,'Welcome')]")).isDisplayed();
        driver.findElement(By.xpath("//div[1]/div[2]/div/a[2]" +
                "[contains(.,'Sign Out')]")).isDisplayed();
        System.out.println("***********     OK      ************");
        System.out.println("");

     
    }

    @After
    public void close() {
        // Commenté pour éviter la fermeture du navigateur pendant le développement.
        // Vous pouvez décommenter cette ligne lorsque vous avez terminé le test.
        // driver.quit();
    }
}
