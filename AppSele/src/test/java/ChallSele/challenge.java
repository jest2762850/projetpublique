package ChallSele;



import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;


public class challenge {

			// Déclaration de la liste qui contiendra les jeux de données
	    ArrayList<Map<String, String>> listJDD;
	    
	    	// Déclaration du driver WebDriver et des éléments WebElements
	    WebDriver driver;
	    WebElement start;
	    WebElement bouton;

	    	// Méthode pour charger les données à partir d'un fichier CSV
	    public ArrayList<Map<String, String>> loadCsvJDD(String fileName) throws IOException {
	        String csvFilePath = "src/test/resources/csv/" + fileName + ".csv";
	        listJDD = new ArrayList<>();
	        List<String[]> list =
	                Files.lines(Paths.get(csvFilePath))
	                        .map(line -> line.split("\\\\r?\\\\n"))
	                        .collect(Collectors.toList());

	        	// Itération sur les lignes du fichier CSV
	        for (int j = 1; j < list.size(); j++) {
	            Map<String, String> jdd = new HashMap<>();
	            String[] titres = list.get(0)[0].split(",");
	            String[] val = list.get(j)[0].split(",");

	            // Remplissage du map avec les données du CSV
	            for (int i = 0; i < titres.length; i++) {
	                jdd.put(titres[i], val[i]);
	            }
	            listJDD.add(jdd);
	        }
	        return listJDD;
	    }

	    
	    
	    @Test
	    public void test() throws Exception {
	    		// Configuration du driver Gecko pour Selenium
	        System.setProperty("webdriver.gecko.driver", "src/test/resources/driver/geckodriver.exe");
	        driver = new FirefoxDriver();

	        System.out.println("************Accès au Site*****************");
	        driver.get("https://www.rpachallenge.com/");
	        driver.manage().window().maximize();
	        
	        	// Localisation et clic sur le bouton de démarrage
	        start = driver.findElement(By.xpath("//div/button[@class='waves-effect col s12 m12 l12 btn-large uiColorButton']"));
	        start.click();

	        	// Chargement des données CSV
	        ArrayList<Map<String, String>> dataList = loadCsvJDD("challenge");

	        	// Itération sur les 10 premières lignes du CSV (comme indiqué dans votre boucle for)
	        for (int i = 0; i < 10; i++) {
	            Map<String, String> data = dataList.get(i);

	            	// Extraction des données du Map
	            String Fname = data.get("First Name");
	            String Lname = data.get("Last Name");
	            String Cname = data.get("Company Name");
	            String Role = data.get("Role in Company");
	            String Adress = data.get("Address");
	            String Email = data.get("Email");
	            String Pnumber = data.get("Phone Number");

	            	// Remplissage des champs de saisie avec les données extraites
	            driver.findElement(By.xpath("//div/input[@ng-reflect-name='labelFirstName']")).sendKeys(Fname);
	            driver.findElement(By.xpath("//div/input[@ng-reflect-name='labelLastName']")).sendKeys(Lname);
	            driver.findElement(By.xpath("//div/input[@ng-reflect-name='labelCompanyName']")).sendKeys(Cname);
	            driver.findElement(By.xpath("//div/input[@ng-reflect-name='labelRole']")).sendKeys(Role);
	            driver.findElement(By.xpath("//div/input[@ng-reflect-name='labelAddress']")).sendKeys(Adress);
	            driver.findElement(By.xpath("//div/input[@ng-reflect-name='labelEmail']")).sendKeys(Email);
	            driver.findElement(By.xpath("//div/input[@ng-reflect-name='labelPhone']")).sendKeys(Pnumber);

	            	// Localisation et clic sur le bouton
	            bouton = driver.findElement(By.xpath("//input[@class='btn uiColorButton']"));
	            bouton.click();
	        }
	    }
	
	
}
