package testWebService;



import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.lang.annotation.Target;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

	public class MouseOver {
		
		WebDriver driver;
		WebElement bouton;
		WebElement source;
		WebElement target;
		WebElement croix;
			
		
		@Test
		public void test() throws InterruptedException {
			System.setProperty("webdriver.gecko.driver" , "src/test/resources/driver/geckodriver.exe");
			driver = new FirefoxDriver();
			
			
			System.out.println("************Acc�s au Site*****************");
			driver.get("http://localhost/TutorialHtml5HotelPhp/");
			driver.manage().window().maximize();
			
			bouton = driver.findElement(By.xpath("/html/body/div[4]/div[2]/div[2]/div[3]/div[3]/div/div[2]/div[1]"));
//			bouton = driver.findElement(By.cssSelector(".//div[@style='left: 0px; top: 0px; width: 40px; height: 50px; position: absolute']"));
			bouton.click();
			driver.switchTo().frame(0);
			driver.findElement(By.xpath("//body/form/h1[contains(.,'New Reservation')]")).isDisplayed();
			driver.findElement(By.name("name")).sendKeys("resa 1");
			bouton = driver.findElement(By.xpath("//div/input[@type='submit']"));
			bouton.click();
			driver.switchTo().defaultContent();
			driver.findElement(By.xpath("//div[@class='scheduler_default_event scheduler_default_event_line0'][contains(.,'resa 1 (12/1/2023 - 12/2/2023)')]")).isDisplayed();
			bouton = driver.findElement(By.xpath("//div[@class='scheduler_default_event scheduler_default_event_line0'][contains(.,'resa 1 (12/1/2023 - 12/2/2023)')]"));
			System.out.println(bouton.getText());
			
			System.out.println("************    OK     ***********");
			System.out.println("");
			
			
			
			
			System.out.println("************supprimer la resa 1****************");
			
			source = driver.findElement(By.xpath("//div[@class='scheduler_default_event scheduler_default_event_line0'][contains(.,'resa 1 (12/1/2023 - 12/2/2023)')]"));
		
			
			
			Actions mouseHover = new Actions(driver);
			mouseHover.moveToElement(source).build().perform();
			Thread.sleep(2000);
	
		   driver.findElement(By.xpath("/html/body/div[4]/div[2]/div[2]/div[3]/div[3]/div/div[9]/div/div[5]")).click();
			
			
		   assertTrue(driver.findElement(By.xpath("//div[@class='scheduler_default_message'][contains(.,'Deleted.')]")).isDisplayed());
		   Thread.sleep(7000);
		   assertFalse(driver.findElement(By.xpath("//div[@class='scheduler_default_message'][contains(.,'Deleted.')]")).isDisplayed());
		   
		   
		   
		   System.out.println("******************          OK           *********************");
			
		}
		
		
		
		
		
	@After
	public void close() {
	
		driver.quit();	
	}
}