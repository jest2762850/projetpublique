package TheInternet;

import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.w3c.dom.Text;

public class Iframe {
	
	
	
	 public void myClick(WebElement element){
	        JavascriptExecutor js=(JavascriptExecutor) driver;
	        js.executeScript("arguments[0].scrollIntoView();",element);
	        js.executeScript("arguments[0].setAttribute('style', 'background: yellow; border: 2px solid red;');", element);
	        
	        element.click();

	    }
	 
	 public void mySendKeys(WebElement element,String text){
	        JavascriptExecutor js=(JavascriptExecutor) driver;
	        js.executeScript("arguments[0].scrollIntoView();",element);
	        js.executeScript("arguments[0].setAttribute('style', 'background: yellow; border: 2px solid red;');", element);
	        element.sendKeys(text);
	 }
	
	
	WebDriver driver;
	WebElement framElement ;
	WebElement iFrame;
	WebElement content;
	WebElement text_area;

	@Before
	
	public void init() {
		System.setProperty("webdriver.gecko.driver" , "src/test/resources/driver/geckodriver.exe");
		driver = new FirefoxDriver();
		System.out.println("************Acc�s au Site*****************");
		driver.get("http://the-internet.herokuapp.com/");
		driver.manage().window().maximize();
	}
	
	@Test
	public void test() throws InterruptedException {
		
	
		framElement = driver.findElement(By.xpath("//ul/li/a[@href='/frames']"));
		myClick(framElement);
		iFrame = driver.findElement(By.xpath("//div/ul/li/a[@href='/iframe']"));
		myClick(iFrame);
		
		driver.switchTo().frame("mce_0_ifr");
		
		content = driver.findElement(By.id("tinymce"));
		
		content.clear();
				
		mySendKeys(content, "Coucou ceci est du texte!");
		
				
		
		}

	
}