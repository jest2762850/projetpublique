package testWebService;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.After;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Tableau {
	
	
	WebDriver driver;
	WebElement bouton;
	WebElement dalmatien;
	WebElement bulldog;
	WebElement chien;
	
	
	
	public WebElement getCellule(int row, int col){
		WebElement element = driver.findElement(By.xpath("//table/tbody/tr["+row+"]/td["+col+"]"));
		return element;
		}
	
	
	public int retournerNumeroDeLigne(String s){
		int ligneCourante = 1;
		List<WebElement> l_lignes = driver.findElements(By.xpath("//table/tbody/tr"));
		for(WebElement ligne : l_lignes){
		List<WebElement> l_cell = ligne.findElements(By.xpath("td"));
		for(WebElement cell:l_cell){
		if(cell.getText().equals(s)){
		return ligneCourante;
		}
		}
		ligneCourante++;
		}
		return -1;
		}

	
	
	@Test
	public void test() {
		System.setProperty("webdriver.gecko.driver" , "src/test/resources/driver/geckodriver.exe");
		driver = new FirefoxDriver();
		
		System.out.println("************Accés au Site*****************");
		
		driver.get("https://petstore.octoperf.com/actions/Catalog.action");
		driver.manage().window().maximize();
		bouton = driver.findElement(By.xpath("//div[1]/div[2]/div/a[2]"));
//		bouton = driver.findElement(By.linkText("Sign In"));
	//	bouton = driver.findElement(By.xpath("//.[@href='/actions/Account.action?signonForm=']"));
		bouton.click();
		driver.findElement(By.name("username")).sendKeys("j2ee");
		driver.findElement(By.name("password")).clear();
		driver.findElement(By.name("password")).sendKeys("j2ee");
		bouton = driver.findElement(By.xpath("//div[2]/div/form/input"));
		bouton.click();
		driver.findElement(By.xpath("//*[@id=\"WelcomeContent\"][contains(.,'Welcome')]")).isDisplayed();
		driver.findElement(By.xpath("//div[1]/div[2]/div/a[2][contains(.,'Sign Out')]")).isDisplayed();
		System.out.println("***********     OK      ************");
		System.out.println("");
		
		
		
		
		System.out.println("*****Test click Dogs*******");
		
		bouton =driver.findElement(By.xpath("//a[@href='/actions/Catalog.action?viewCategory=&categoryId=DOGS']"));
		bouton.click();
		//Vérification
		driver.findElement(By.xpath("/html/body/div[2]/div[2]/h2[contains(.,'Dogs')]")).isDisplayed();
		System.out.println("************    OK     ***********");
		System.out.println("");
		
		
		System.out.println("*****Test parcours liste  et click Dalmatien*******");
		
		dalmatien = getCellule(retournerNumeroDeLigne("Dalmation"), 1);
		dalmatien.click();	
		//Vérification
		driver.findElement(By.xpath("/html/body/div[2]/div[2]/h2[contains(.,'Dalmation')]")).isDisplayed();
		System.out.println("************    OK     ***********");
		System.out.println("");
		
		
		System.out.println("*****Test changement via modif BDD vers Bulldogs*******");
		driver.navigate().back();
		bulldog = getCellule(retournerNumeroDeLigne("Bulldog"), 1);
		bulldog.click();	
		
	//Vérification
		driver.findElement(By.xpath("/html/body/div[2]/div[2]/h2[contains(.,'Bulldog')]")).isDisplayed();
		System.out.println("************    OK     ***********");
		System.out.println("");
		
		
		
		System.out.println("*****Test click Dogs*******");
		
		bouton =driver.findElement(By.xpath("//a[@href='/actions/Catalog.action?viewCategory=&categoryId=DOGS']"));
		bouton.click();
		//Vérification
		driver.findElement(By.xpath("/html/body/div[2]/div[2]/h2[contains(.,'Dogs')]")).isDisplayed();
		System.out.println("************    OK     ***********");
		System.out.println("");
		
		
		System.out.println("*****Test parcours liste  et click Dalmatien*******");
		
		dalmatien = getCellule(retournerNumeroDeLigne("Dalmation"), 1);
		dalmatien.click();	
		//Vérification
		driver.findElement(By.xpath("/html/body/div[2]/div[2]/h2[contains(.,'Dalmation')]")).isDisplayed();
		System.out.println("************    OK     ***********");
		System.out.println("");
	}
		
		
		
		
		
	@After
	public void close() {
	
		driver.quit();	
	}

}


